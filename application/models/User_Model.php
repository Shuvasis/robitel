<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model {

	private $table = 'users';
	private $user_info_table = 'user_info';
	private $user_group_table = 'user_group';

	// get all users data
	public function get_users() {
		return $this->db->join($this->user_info_table, $this->user_info_table.'.user_id = '. $this->table .'.id')
						->join($this->user_group_table, $this->user_group_table. '.id = '. $this->user_info_table .'.ugroup_id')
						->order_by('user_status', 'DESC')
						->order_by('ugroup_name', 'ASC')
						->get($this->table)
						->result();		
	}

	// get all user data of user with given user_id
	public function get_user($id) {
		$result = $this->db->where($this->table.'.id', $id)
							->join($this->user_info_table, $this->user_info_table. '.user_id = '. $this->table .'.id')
							->join($this->user_group_table, $this->user_group_table. '.id = '. $this->user_info_table .'.ugroup_id')
							->get($this->table)
							->result();
		if ($result) {
			return $result[0];
		} else {
			return False;
		}	
	}

	// get all user groups from user_group table
	public function get_user_groups() {
		$result = $this->db->get($this->user_group_table)->result();
		if ($result) {
			return $result;
		} else {
			return False;
		}

	}

	// get all data of user if exists
	public function user_exist($data) {
		if ($result = $this->db->where($data)->get($this->table)->result()) {
			return $this->get_user($result[0]->id);
		} else {
			return False;
		}	
	}

	// insert all user data of new user
	public function create_user($data_1, $data_2) {
		// insert user's user_email & user_password in users table
		$this->db->insert($this->table, $data_1);
		
		//get the user_id from users table with this user_email
		$user_id = $this->db->select('id')
							->where('user_email', $data_1['user_email'])
							->get($this->table)
							->result()[0]
							->id;
		
		// include user_id in data_2 array for this newly created user
		$data_2['user_id'] = $user_id;

		// insert users' other info -> user_id, user_name, user_phone, user_address, user_img in users_info table
		$this->db->insert($this->user_info_table, $data_2);
	}

	// update all user data of user with given user_id & the given data
	public function update_user($id, $data_1, $data_2) {
		// update user_status in users table
		$this->db->where('id', $id)
				 ->update($this->table, $data_1);
		
		// update users' other info -> user_id, user_group_id, user_name , user_phone, user_address, user_img in users_info table
		$this->db->where('user_id', $id)
				 ->update($this->user_info_table, $data_2);
	}

	// delete all user data of user with given user_id
	public function delete_user($id) {
		$this->db->where('id', $id)
				 ->delete($this->table);
	}

	// change password of user with given user_id
	public function change_password($id, $new_pass) {
		$this->db->where('id', $id)
				 ->update($this->table, $new_pass);
	}
}
?>