<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Package_Model extends CI_Model {

	private $table = 'packages';

	// public function create_class() {
	// 	$this->db->query('CREATE TABLE IF NOT EXIST `robitel`.`packages` ( `package_id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `package_name` TEXT NOT NULL , `package_avg_speed` TEXT NOT NULL , `package_youtube_speed` TEXT NOT NULL , `package_price` TEXT NOT NULL , `package_img_src` TEXT NOT NULL , PRIMARY KEY (`package_id`)) ENGINE = InnoDB;');
	// }

	public function get_packages() {
		return $this->db->get($this->table)->result();
	}

	public function get_package($id) {
		$result =  $this->db->where('id', $id)
						->get($this->table)
						->result();
		if ($result) {
			return $result[0];
		} else {
			return False;
		}
	}

	public function create_package($data) {
		$this->db->insert($this->table, $data);
	}

	public function update_package($id, $data) {
		$this->db->where('id', $id)
				 ->update($this->table, $data);
	}

	public function delete_package($id) {
		$this->db->where('id', $id)
				 ->delete($this->table);
	}
}

?>