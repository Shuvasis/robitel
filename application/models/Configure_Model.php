<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Configure_Model extends CI_Model {
	
	private $table = 'meta';

	// gets all data from meta table
	public function get_metadata() {
		$result = $this->db->get($this->table)->result();

		foreach ($result as $row) {
			$metadata[$row->meta_key] = $row->meta_value;
		}

		return (object)$metadata;
	}
	
	// insert or updates the data of meta table
	public function replace_metadata($datas) {
		foreach ($datas as $data) { 
			$this->db->replace($this->table, $data);
		}
	}
}