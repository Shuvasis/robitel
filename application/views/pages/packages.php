<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	// include Header
	$this->load->view('template/header');
?>	
<!-- Content -->
<div class="main-content">
	<div class="package container text-center">
		<h1 class="page-title"><?php echo $title; ?></h1>
		<div class="row">
			<?php foreach($packages as $package) : ?>
			<div class="col-lg-4 col-sm-6">
				<div class="card bg-warning">
					<img class="card-img-top" src="<?php echo $package->package_thumbnail; ?>" alt="Card image cap">
					<div class="card-body">
						<h4 class="card-title text-center"><?php echo $package->package_name; ?></h4>
						<p class="card-text"><?php echo $package->package_description; ?></p>
					</div><!-- ./card-body -->
					<ul class="list-group list-group-flush text-center">
					    <li class="list-group-item">
					    	<div class="row">
						    	<div class="col-lg-6">Speed</div>
						    	<div class="col-lg-6"><?php echo $package->package_avg_speed; ?></div>
					    	</div>
					    </li>
					     <li class="list-group-item">
					    	<div class="row">
						    	<div class="col-lg-6">Youtube Speed</div>
						    	<div class="col-lg-6"><?php echo $package->package_youtube_speed; ?></div>
					    	</div>
					    </li>
					     <li class="list-group-item">
					    	<div class="row">
						    	<div class="col-lg-6">Price</div>
						    	<div class="col-lg-6"><?php echo $package->package_price; ?> BDT</div>
					    	</div>
					    </li>
	  				</ul>
	  				<div class="card-footer text-muted text-center">
	  					<a href="#" class="btn btn-primary">Order</a>
	  				</div><!-- ./card-footer-->
				</div><!-- ./card -->
			</div><!-- ./col-lg-4 -->		
		<?php endforeach; ?>
		</div>
	</div><!-- ./package -->
</div><!-- ./main-content -->

<!-- End Content -->

<?php
	// include Footer
	$this->load->view('template/footer');
?>