<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	// include Header
$this->load->view('template/header');
?>	

<!-- Content -->
<div class="main-content">
	<div class="container page">
		<h1 class="page-title"><?php echo $title; ?></h1>

	</div><!-- ./page -->
</div><!-- ./main-content -->
<!-- End Content -->

<?php
	// include Footer
$this->load->view('template/footer');
?>