<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

// include header
$this->load->view('template/header');
?>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div class="com-lg-6"></div>	
			<div class="com-lg-6"></div>	
		</div><!-- ./row -->
		<div class="row">
			<div class="com-lg-6"></div>	
			<div class="com-lg-6"></div>	
		</div><!-- ./row -->
		<div class="row">
			<div class="com-lg-6"></div>	
			<div class="com-lg-6"></div>	
		</div><!-- ./row -->
	</div><!-- ./container -->
</div><!-- ./main-content -->


<?php
// include footer
$this->load->view('template/footer');
?>
