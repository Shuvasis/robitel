<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	// include Header
$this->load->view('template/header');
?>	

<!-- Content -->
<div class="main-content">
	<div class="container contact">
		<h1 class="page-title"><?php echo $title; ?></h1>
		<div class="row">
			<div class="col-lg-6 email">
				<div class="card">
					<div class="card-header">Email Us</div><!-- ./card-header -->
					<div class="card-body">
						<?php 
						$user_email_attr = array(
							'class' 		=> 'form-control',
							'name'			=> 'user_email',
							'placeholder'	=> 'Your Email'
						);

						$user_subject_attr = array(
							'class' 		=> 'form-control',
							'name'			=> 'user_subject',
							'placeholder'	=> 'Subject'
						);

						$user_message_attr = array(
							'rows'			=> '8',
							'class' 		=> 'form-control',
							'name'			=> 'user_message',
							'placeholder'	=> 'Your Message'
						);

						$submit_attr = array(
							'type'			=> 'submit',
							'class' 		=> 'form-control btn btn-primary',
							'name'			=> 'submit_email',
							'value'	=> 'Send'
						);
						?>
						<?php echo form_open('page/'); ?>
						<div class="form-group">
							<?php echo form_input($user_email_attr); ?>
							<span class="error"></span>	
						</div><!-- ./input-group -->	
						<div class="form-group">
							<?php echo form_input($user_subject_attr); ?>	
							<span class="error"></span>
						</div><!-- ./input-group -->
						<div class="form-group">
							<?php echo form_textarea($user_message_attr); ?>	
							<span class="error"></span>
						</div><!-- ./input-group -->
					</div><!-- ./card-body -->
					<div class="card-footer">
						<?php echo form_submit($submit_attr); ?>
						<?php echo form_close(); ?>
					</div><!-- ./card-footer -->
				</div><!-- ./card -->
			</div><!-- ./email -->
			<div class="col-lg-6 contact-address">
				<div class="card">
					<div class="card-header">Contact</div><!-- ./card-header -->
					<img class="card-img-top" src="<?php echo base_url('assets/img/download.svg'); ?>" alt="Card image cap">
					<div class="card-body">
						<ul class="list-group">
							<li class="list-group-item">
								<div class="row">
									<div class="col-lg-1 icon"><i class="fa fa-phone"></i></div><!-- ./icon -->
									<div class="col-lg-11 info">3rd Floor, Rijuan Complex, Riajuddin Bazar, New Market, Chittagong</div><!-- ./info -->
								</div><!-- ./row -->
							</li>
							<li class="list-group-item">
								<div class="row">
									<div class="col-lg-1 icon"><i class="fa fa-phone"></i></div><!-- ./icon -->
									<div class="col-lg-11 info">017XXXXXXXX</div><!-- ./info -->
								</div><!-- ./row -->
							</li>
							<li class="list-group-item">
								<div class="row">
									<div class="col-lg-1 icon"><i class="fa fa-phone"></i></div><!-- ./icon -->
									<div class="col-lg-11 info">admin@company.com</div><!-- ./info -->
								</div><!-- ./row -->
							</li>
						</ul>
					</div><!-- ./card-body -->
				</div><!-- ./card -->
			</div><!-- ./contact-address -->
		</div><!-- ./row -->
		<div class="row">
			<div class="map"></div>
		</div><!-- ./row -->
	</div><!-- ./contact -->
</div><!-- ./main-content -->
<!-- End Content -->

<?php
	// include Footer
$this->load->view('template/footer');
?>