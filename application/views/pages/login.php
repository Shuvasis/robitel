<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	// include Header
$this->load->view('template/header');
?>	

<!-- Content -->
<div class="main-content login-page">
	<div class="container">
		<div class="row">
			<div class="offset-lg-4 col-lg-4 offset-lg-4 login-panel">
				<div class="card">
					<div class="card-header">
						<h1 class="page-title"><?php echo $title; ?></h1>
					</div><!-- ./card-header -->
					<div class="card-body">
						<?php if (isset($errors)) : ?>
							<div class="form-msg-box">
								<?php echo $errors; ?>
							</div><!-- ./form-msg-box -->
						<?php endif;

						$user_email_attr = array(
							'class'			=> 'form-control',
							'name'			=> 'user_email',
							'placeholder'	=> 'Your Email',
							'value'			=> set_value('user_email')
						); 

						$user_password_attr = array(
							'type'			=> 'password',
							'class'			=> 'form-control',
							'name'			=> 'user_password',
							'placeholder'	=> 'Your Password',
							'value'			=> set_value('user_password')
						);

						$submit_attr = array(
							'type'			=> 'submit',
							'class'			=> 'form-control btn btn-primary border-0',
							'name'			=> 'submit_login_form',
							'value'			=> 'Login'
						);
						?>
						<?php echo form_open('user/login'); ?>
						<div class="form-group">
							<?php echo form_input($user_email_attr); ?>
						</div>
						<div class="form-group">
							<?php echo form_input($user_password_attr); ?>
						</div>
						<div class="form-group">
							<?php echo form_submit($submit_attr); ?>
						</div>
						<?php echo form_close(); ?>
						<p class="text-center">Not a member? Get <a href="<?php echo base_url('user/register'); ?>">Register</a>
						</p>
						<p class="text-center"><a href="<?php echo base_url('page/forgot_password'); ?>">Forgot password?</a>
						</p>
					</div><!-- ./card-body -->
				</div><!-- ./card -->
			</div><!-- ./login-panel -->
		</div><!-- ./row -->
	</div><!-- ./container -->
</div><!-- ./main-content -->
<!-- End Content -->

<?php
	// include Footer
$this->load->view('template/footer');
?>