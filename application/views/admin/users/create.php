<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

// include header
$this->load->view('template/header');
?>	

<!-- Content -->
<div class="main-content register-page">
	<div class="container">
		<div class="row">
			<div class="offset-lg-3 col-lg-6 offset-lg-3 login-panel">
				<div class="card">
					<div class="card-header">
						<h1 class="page-title"><?php echo $title; ?></h1>
					</div><!-- ./card-header -->
					<div class="card-body">
						<?php if (isset($errors)) : ?>
							<div class="form-msg-box">
								<?php echo $errors; ?>
							</div><!-- ./form-msg-box -->
						<?php endif;
						
						$user_email_attr = array(
							'class'			=> 'form-control',
							'name'			=> 'user_email',
							'placeholder'	=> 'Your Email',
							'value'			=> set_value('user_email')
						); 

						$user_password_attr = array(
							'type'			=> 'password',
							'class'			=> 'form-control',
							'name'			=> 'user_password',
							'placeholder'	=> 'Your Password',
							'value'			=> set_value('user_password')
						);

						$user_confirm_password_attr = array(
							'type'			=> 'password',
							'class'			=> 'form-control',
							'name'			=> 'user_confirm_password',
							'placeholder'	=> 'Confirm Password',
							'value'			=> set_value('user_confirm_password')
						);

						$user_name_attr = array(
							'class'			=> 'form-control',
							'name'			=> 'user_name',
							'placeholder'	=> 'Your Name',
							'value'			=> set_value('user_name')
						);

						$user_phone_attr = array(
							'class'			=> 'form-control',
							'name'			=> 'user_phone',
							'placeholder'	=> 'Your Phone No.',
							'value'			=> set_value('user_phone')
						);

						$user_address_attr = array(
							'rows'			=> '5',
							'class'			=> 'form-control',
							'name'			=> 'user_address',
							'placeholder'	=> 'Your Present Address',
							'value'			=> set_value('user_address')
						);

						$user_img_attr = array(
							'type'			=> 'file',
							'class'			=> 'form-control',
							'name'			=> 'user_img',
							'value'			=> set_value('user_img')
						);


						$submit_attr = array(
							'type'			=> 'submit',
							'class'			=> 'form-control btn btn-primary border-0',
							'name'			=> 'submit_register_form',
							'value'			=> 'Register'
						);
						?>
						<?php echo form_open('user/register'); ?>
						<div class="form-group">
							<?php echo form_input($user_email_attr); ?>
						</div>
						<div class="form-group">
							<?php echo form_input($user_password_attr); ?>
						</div>
						<div class="form-group">
							<?php echo form_input($user_confirm_password_attr); ?>
						</div>
						<div class="form-group">
							<?php echo form_input($user_name_attr); ?>
						</div>
						<div class="form-group">
							<?php echo form_input($user_phone_attr); ?>
						</div>
						<div class="form-group">
							<?php echo form_textarea($user_address_attr); ?>
						</div>
						<div class="form-group input-group">
							<span class="input-group-addon" id="basic-addon1">Your Image</span>
							<?php echo form_input($user_img_attr); ?>
						</div>
						<div class="form-group">
							<?php echo form_submit($submit_attr); ?>
						</div>
						<?php echo form_close(); ?>
						<p class="text-center">Already a member? Please <a href="<?php echo base_url('user/login'); ?>">Login</a>
						</p>
					</div><!-- ./card-body -->
				</div><!-- ./card -->
			</div><!-- ./login-panel -->
		</div><!-- ./row -->
	</div><!-- ./container -->
</div><!-- ./main-content -->
<!-- End Content -->

<?php
// include footer
$this->load->view('template/footer');
?>