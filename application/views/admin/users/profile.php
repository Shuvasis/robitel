<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

// include header
$this->load->view('template/header');

// include dashboard_start
$this->load->view('template/dashboard_start');
?>	

<!-- Content -->
<div class="main-content dashboard-page">
	<div class="container user-profile-page">
		<div class="card user">
			<div class="card-header text-center">
				<h1 class="page-title">User Profile</h1>
			</div><!-- ./card-header -->
			<div class="card-body">
				<div class="text-center">
					<img class="card-img-top img-fluid" src="<?php if(isset($user->user_img)) echo $user->user_img; ?>" alt="Card image cap">
				</div></br>
				<ul class="list-group">
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Name</div>
							<div class="col-lg-10"><?php if(isset($user->user_name)) echo $user->user_name; ?></div>
						</div><!-- ./row -->
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Email</div>
							<div class="col-lg-10"><?php if(isset($user->user_email)) echo $user->user_email; ?></div>
						</div><!-- ./row -->
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Phone</div>
							<div class="col-lg-10"><?php if(isset($user->user_phone)) echo $user->user_phone; ?></div>
						</div><!-- ./row -->
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Address</div>
							<div class="col-lg-10"><?php if(isset($user->user_address)) echo $user->user_address; ?></div>
						</div><!-- ./row -->
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Group</div>
							<div class="col-lg-10"><?php if(isset($user->ugroup_name)) echo $user->ugroup_name; ?></div>
						</div><!-- ./row -->
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Status</div>
							<div class="col-lg-10"><?php if(isset($user->user_status)) echo $user->user_status; ?></div>
						</div><!-- ./row -->
					</li>
				</ul>
			</div><!-- ./card-body -->
			<div class="card-footer text-center">
				<a href="<?php echo base_url('dashboard/user_edit/'. (isset($user->user_id)? $user->user_id: '') ); ?>" class="col-lg-4 btn submit-btn">Edit</a>
			</div><!-- ./card-footer -->
		</div><!-- ./card -->
			
	</div><!-- ./container -->
</div><!-- ./main-content -->
<!-- End Content -->

<?php
// include dashboard_end
$this->load->view('template/dashboard_end');

// include footer
$this->load->view('template/footer');
?>