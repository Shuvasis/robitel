<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

// include header
$this->load->view('template/header');

// include dashboard_start
$this->load->view('template/dashboard_start');
?>

<!-- Content -->
<div class="main-content dashboard-page">
	<div class="container user-edit-page">
		<div class="card user">
			<div class="card-header text-center">
				<h1 class="page-title">Edit User</h1>
			</div><!-- ./card-header -->
			<div class="card-body">
				<div class="text-center">
					<img class="card-img-top img-fluid" src="<?php echo isset($user->user_img)? $user->user_img : ''; ?>" alt="Card image cap">
				</div>
				<?php if (isset($errors)) : ?>
					<div class="form-msg-box">
						<?php echo $errors; ?>
					</div><!-- ./form-msg-box -->
				<?php endif;

				$user_name_attr = array(
					'class'			=> 'form-control',
					'name'			=> 'user_name',
					'placeholder'	=> 'Your Name',
					'value'			=> set_value('user_name')? set_value('user_name'): isset($user->user_name)? $user->user_name : ''
				);

				$user_phone_attr = array(
					'class'			=> 'form-control',
					'name'			=> 'user_phone',
					'placeholder'	=> 'Your Phone No.',
					'value'			=> set_value('user_phone')? set_value('user_phone'): isset($user->user_phone)? $user->user_phone : ''
				);

				$user_address_attr = array(
					'rows'			=> '5',
					'class'			=> 'form-control',
					'name'			=> 'user_address',
					'placeholder'	=> 'Your Present Address',
					'value'			=> set_value('user_address')? set_value('user_address'): isset($user->user_address)? $user->user_address : '' 
				);
				
				$user_status_attr = array(
					'class'			=> 'form-control',
					'name'			=> 'user_status',
					'selected'		=> set_value('user_status')? set_value('user_status'): isset($user->user_status)? $user->user_status : ''
				);

				// Get only groups from all user_group objects as they are send as objects array
				foreach ($user_groups as $key => $value) {
						$groups[$value->id] = $value->ugroup_name;
				}	

				$user_group_options = $groups;
				$user_group_selected = array(set_value('ugroup_id')? set_value('ugroup_id'): isset($user->ugroup_id)? $user->ugroup_id : '');
					
				$user_status_options = array('Active' => 'Active', 'Pending' => 'Pending');
				$user_status_selected = array(set_value('user_status')? set_value('user_status'): isset($user->user_status)? $user->user_status : '');

				$user_img_attr = array(
					'type'			=> 'file',
					'class'			=> 'form-control',
					'name'			=> 'user_img',
					'value'			=> set_value('user_img')? set_value('user_img'): isset($user->user_img)? $user->user_img : ''
				);


				$submit_attr = array(
					'type'			=> 'submit',
					'class'			=> 'form-control btn submit-btn border-0 offset-lg-4 col-lg-4 offset-lg-4',
					'name'			=> 'submit_user_edit_form',
					'value'			=> 'Update'
				);

				echo form_open('dashboard/user_edit/'. (isset($user->user_id)? $user->user_id : '') ); 
				?>
				
				<ul class="list-group">
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Name</div>
							<div class="col-lg-10"><?php echo form_input($user_name_attr); ?></div>
						</div><!-- ./row -->
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Phone</div>
							<div class="col-lg-10"><?php echo form_input($user_phone_attr); ?></div>
						</div><!-- ./row -->
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Address</div>
							<div class="col-lg-10"><?php echo form_input($user_address_attr); ?></div>
						</div><!-- ./row -->
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Group</div>
							<div class="col-lg-10"><?php echo form_dropdown('ugroup_id', $user_group_options, $user_group_selected, $extra= array('class' => 'form-control')); ?></div>
						</div><!-- ./row -->
					</li>
					<li class="list-group-item">
						<div class="row">
							<div class="col-lg-2 tag">Status</div>
							<div class="col-lg-10"><?php echo form_dropdown($name= 'user_status', $user_status_options, $user_status_selected, $extra= array('class' => 'form-control')); ?></div>
						</div><!-- ./row -->
					</li>
				</ul>
			</div><!-- ./card-body -->
			<div class="card-footer">
				<?php echo form_submit($submit_attr); ?>
				<?php form_close(); ?>
			</div><!-- ./card-footer -->
		</div><!-- ./card -->

	</div><!-- ./container -->
</div><!-- ./main-content -->
<!-- End Content -->

<?php
// include dashboard_end
$this->load->view('template/dashboard_end');

// include footer
$this->load->view('template/footer');
?>