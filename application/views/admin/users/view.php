<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

// include header
$this->load->view('template/header');

// include dashboard_start
$this->load->view('template/dashboard_start');
?>

<!-- Content -->
<div class="main-content dashboard-page">
	<div class="container">
			<h1 class="text-center page-title"><?php echo $title; ?></h1>
			<div class="text-center">
				<a href="<?= base_url('dashboard/user_add'); ?>" class="btn btn-primary">Add User</a>
			</div></br>
			<div class="user_view">
				<table class="table table-responsive-lg table-striped table-hover">
					<thead class="thead-dark">
						<tr>
							<th scope="col">No</th>
							<th scope="col">User Name</th>
							<th scope="col">User Email</th>
							<th scope="col">User Phone</th>
							<th scope="col">User Status</th>
							<th scope="col">User Group</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
			
					<?php $counter = 0; ?>	
					<?php foreach($users as $user) : ?>
					<tbody class="">
						<tr>
							<th scope="row"><?php echo ++$counter; ?></td>
							<td><?php echo $user->user_name; ?></td>
							<td><?php echo $user->user_email; ?></td>
							<td><?php echo $user->user_phone;?></td>
							<td><?php echo $user->user_status;?></td>
							<td><?php echo $user->ugroup_name;?></td>
							<td>
								<a href="<?php echo base_url('dashboard/user_profile/'. $user->user_id); ?>" class="btn btn-sm btn-primary">View</a>						<a href="<?php echo base_url('dashboard/user_edit/'. $user->user_id); ?>" class="btn btn-sm btn-primary">Edit</a>
								<a href="<?php echo base_url('dashboard/user_delete/'. $user->user_id); ?>" class="btn btn-sm btn-primary">Delete</a>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
			</div><!-- ./table -->	
	</div><!-- ./container -->
</div><!-- ./main-content -->
<!-- End Content -->

<?php
// include dashboard_end
$this->load->view('template/dashboard_end');

// include footer
$this->load->view('template/footer');
?>