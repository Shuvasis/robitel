<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

// include header
$this->load->view('template/header');

// include dashboard_start
$this->load->view('template/dashboard_start');
?>

<!-- Dashboard Content -->
<div class="main-content">
	<div class="container dashboard-page">
		<p>Dashboard Content</p>
	</div><!-- ./dashboard-page -->
</div><!-- ./main-content -->

<?php
// include dashboard_end
$this->load->view('template/dashboard_end');

// include footer
$this->load->view('template/footer');
?>