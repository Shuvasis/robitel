<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

// include header
$this->load->view('template/header');

// include dashboard_start
$this->load->view('template/dashboard_start');
?>

<!-- Dashboard Content -->
<div class="main-content dashboard-page">
	<div class="container faq-page">
		<div class="card">
			<div class="card-header">
				<h1 class="page-title">Create FAQ</h1>
			</div><!-- ./card-header -->
			<div class="card-body">
				<?php
				$question_attr = array(
					'rows'			=> '2',
					'class'			=> 'form-control',
					'name'			=> 'question',
					'placeholder'	=> 'Question',
					'value'			=> set_value('')? set_value('question') : isset($faq->question)? $faq->question : ''
				);

				$answer_attr = array(
					'rows'			=> '3',
					'class'			=> 'form-control',
					'name'			=> 'answer',
					'placeholder'	=> 'Answer',
					'value'			=> set_value('answer')? set_value('answer') : isset($faq->answer)? $faq->answer : ''
				);

				$submit_attr = array(
					'class'	=> 'form-control btn submit-btn col-lg-4',
					'name'	=> 'submit',
					'value'	=> 'Create'
				);

				echo form_open('dashboard/create_faq');
				?>
				<ul class="list-group">
					<li class="list-group-item row">
						<div class="row">
							<div class="col-lg-1"><?php echo form_label('Question'); ?></div>
							<div class="col-lg-11"><?php echo form_textarea($question_attr) ?></div>
						</div><!-- ./row -->
						</br>
						<div class="row">
							<div class="col-lg-1"><?php echo form_label('Answer'); ?></div>
							<div class="col-lg-11"><?php echo form_textarea($answer_attr) ?></div>
						</div><!-- ./row -->
					</li>
				</ul><!-- ./list-group -->
			</div><!-- ./card-body -->
			<div class="card-footer text-center">
				<?php 
				echo form_submit($submit_attr);
				echo form_close();
				?>
			</div><!-- ./card-footer -->
		</div><!-- ./card -->
	</div><!-- ./configure-page -->
</div><!-- ./main-content -->

<?php
// include dashboard_end
$this->load->view('template/dashboard_end');

// include footer
$this->load->view('template/footer');
?>