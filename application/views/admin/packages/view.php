<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// include Header
$this->load->view('template/header');

// include dashboard_start
$this->load->view('template/dashboard_start');
?>	

<!-- Content -->
<div class="main-content dashboard-page">
	<div class="container">
			<h1 class="text-center page-title"><?php echo $title; ?></h1>
			<div class="text-center mb-3">
				<a class="btn btn-info" href="<?php echo base_url('dashboard/create_package'); ?>">Creat A New Package</a>
			</div>
			<div class="user_view">
				<table class="table table-responsive-lg table-striped table-hover">
					<thead class="thead-dark">
						<tr>
							<th scope="col">No</th>
							<th scope="col">Package Name</th>
							<th scope="col">Package Speed</th>
							<th scope="col">Package Price</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
			
					<?php $counter = 0; ?>	
					<?php foreach($packages as $package) : ?>
					<tbody class="">
						<tr>
							<th scope="row"><?php echo ++$counter; ?></td>
							<td><?php echo $package->package_name; ?></td>
							<td><?php echo $package->package_avg_speed; ?></td>
							<td><?php echo $package->package_price;?></td>
							<td>
								<a href="<?php echo base_url('dashboard/package_overview/'. $package->id); ?>" class="btn btn-primary">View</a>						<a href="<?php echo base_url('dashboard/edit_package/'. $package->id); ?>" class="btn btn-primary">Edit</a>
								<a href="<?php echo base_url('dashboard/delete_package/'. $package->id); ?>" class="btn btn-primary">Delete</a>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
			</div><!-- ./table -->	
	</div><!-- ./container -->
</div><!-- ./main-content -->
<!-- End Content -->

<?php
// include dashboard_end
$this->load->view('template/dashboard_end');

// include footer
$this->load->view('template/footer');
?>