<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// include Header
$this->load->view('template/header');

// include dashboard_start
$this->load->view('template/dashboard_start');
?>
<!-- Content -->
<div class="main-content dashboard-page">
	<div class="container create-package-page">
		<div class="card package">
			<div class="card-header">
				<h1 class="text-center page-title"><?php echo $title; ?></h1>
			</div><!-- ./card-header -->
			<div class="card-body">
				<?php if (isset($errors)) : ?>
					<div class="form-msg-box">
						<?php echo $errors; ?>
					</div><!-- ./form-msg-box -->
				<?php endif;

				$package_name_attr = array(
					'class' 		=> 'form-control',
					'name' 			=> 'package_name',
					'placeholder'	=> 'Package Name',
					'value'			=> set_value('package_name')	
				);
				$package_description_attr = array(
					'rows'			=> '3',
					'class' 		=> 'form-control',
					'name' 			=> 'package_description',
					'placeholder'	=> 'Package Description',
					'value'			=> set_value('package_description')
				);
				$package_avg_speed_attr = array(
					'class' 		=> 'form-control',
					'name' 			=> 'package_avg_speed',
					'placeholder'	=> 'Speed',
					'value'			=> set_value('package_avg_speed')
				);
				$package_youtube_speed_attr = array(
					'class' 		=> 'form-control',
					'name' 			=> 'package_youtube_speed',
					'placeholder'	=> 'Youtube Speed',
					'value'			=> set_value('package_youtube_speed')
				);
				$package_price_attr = array(
					'class' 		=> 'form-control',
					'name' 			=> 'package_price',
					'placeholder'	=> 'Package Price',
					'value'			=> set_value('package_price')	
				);
				$package_thumbnail_attr = array(
					'class' 		=> 'form-control',
					'name' 			=> 'package_thumbnail',
					'placeholder'	=> 'Package Thumbnail Image',
					'value'			=> set_value('package_thumbnail')	
				);
				$submit_attr = array(
					'type'	=> 'submit',
					'class' => 'form_control btn btn-primary offset-lg-4 col-lg-4 offset-lg-4',
					'name' 	=> 'submit_create_package',
					'value'	=> 'Create'			
				);

				echo form_open_multipart('dashboard/create_package'); 
				echo form_input($package_name_attr) . '</br>';
				echo form_textarea($package_description_attr) . '</br>';
				echo form_input($package_avg_speed_attr) . '</br>';
				echo form_input($package_youtube_speed_attr) . '</br>';
				echo form_input($package_price_attr) . '</br>';
				?>
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">Package Thumbnail Image</span>
				<?php
				echo form_upload($package_thumbnail_attr) . '</br>';
				?>
				</div><!-- ./input-group -->				
			</div><!-- ./card-body -->
			<div class="card-footer">
				<?php
				echo form_submit($submit_attr);
				echo form_close();	
				?>
			</div><!-- ./card-footer -->
		</div><!-- ./package_form -->
	</div><!-- ./container -->	
</div><!-- ./main-content -->

<!-- End Content -->

<?php
// include dashboard_end
$this->load->view('template/dashboard_end');

// include footer
$this->load->view('template/footer');
?>