<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

// include header
$this->load->view('template/header');

// include dashboard_start
$this->load->view('template/dashboard_start');
?>

<!-- Content -->
<div class="main-content dashboard-page">
	<div class="container single-package-page">
		<div class="card package">
			<img class="card-img-top" src="<?php echo $package->package_thumbnail; ?>" alt="Card image cap">
			<div class="card-body">
				<h4 class="card-title"><?php echo $package->package_name; ?></h4>
				<p class="card-text"><?php echo $package->package_description; ?></p>
			</div><!-- ./card-body -->
			<ul class="list-group list-group-flush text-center">
				<li class="list-group-item">
					<div class="row">
						<div class="col-lg-6">Speed</div>
						<div class="col-lg-6"><?php echo $package->package_avg_speed; ?></div>
					</div>
				</li>
				<li class="list-group-item">
					<div class="row">
						<div class="col-lg-6">Youtube Speed</div>
						<div class="col-lg-6"><?php echo $package->package_youtube_speed; ?></div>
					</div>
				</li>
				<li class="list-group-item">
					<div class="row">
						<div class="col-lg-6">Price</div>
						<div class="col-lg-6"><?php echo $package->package_price; ?> BDT</div>
					</div>
				</li>
			</ul>
			<div class="card-footer text-center">
				<a href="<?php echo base_url('dashboard/edit_package/'. (isset($package->id)? $package->id: '')); ?>" class="form-control col-lg-5 btn submit-btn">Edit</a>
			</div>
		</div><!-- ./card -->
	</div><!-- ./container -->
</div><!-- ./main-content -->
<!-- End Content -->

<?php
// include dashboard_end
$this->load->view('template/dashboard_end');

// include footer
$this->load->view('template/footer');
?>