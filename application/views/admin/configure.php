<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

// include header
$this->load->view('template/header');

// include dashboard_start
$this->load->view('template/dashboard_start');
?>

<!-- Dashboard Content -->
<div class="main-content dashboard-page">
	<div class="container configure-page">
		<div class="card">
			<div class="card-header">
				<h1 class="page-title">Configure</h1>
			</div><!-- ./card-header -->
			<div class="card-body">
				<?php
				$company_name_attr = array(
					'class'			=> 'form-control',
					'name'			=> 'company_name',
					'placeholder'	=> 'Company Name',
					'value'			=> set_value('company_name')? set_value('company_name') : isset($configure->company_name)? $configure->company_name : ''
				);

				$company_email_attr = array(
					'class'			=> 'form-control',
					'name'			=> 'company_email',
					'placeholder'	=> 'Company Email',
					'value'			=> set_value('company_email')? set_value('company_email') : isset($configure->company_email)? $configure->company_email : ''
				);

				$company_phone_attr = array(
					'class'			=> 'form-control',
					'name'			=> 'company_phone',
					'placeholder'	=> 'Company Phone',
					'value'			=> set_value('company_phone')? set_value('company_phone') : isset($configure->company_phone)? $configure->company_phone : ''
				);

				$company_address_attr = array(
					'rows'			=> '5',
					'class'			=> 'form-control',
					'name'			=> 'company_address',
					'placeholder'	=> 'Company Address',
					'value'			=> set_value('company_address')? set_value('company_address') : isset($configure->company_address)? $configure->company_address : ''
				);

				$company_google_map_attr = array(
					'rows'			=> '3',
					'class'			=> 'form-control',
					'name'			=> 'company_google_map',
					'placeholder'	=> 'Company Google Map',
					'value'			=> set_value('company_google_map')? set_value('company_google_map') : isset($configure->company_google_map)? $configure->company_google_map : ''
				);

				$submit_configure_attr = array(
					'class'	=> 'form-control btn submit-btn col-lg-4',
					'name'	=> 'submit_configure',
					'value'	=> 'Update'
				);

				echo form_open('dashboard/configure');
				?>
				<ul class="list-group">
					<li class="list-group-item row">
						<div class="row">
							<div class="col-lg-3"><?php echo form_label('Company Name'); ?></div>
							<div class="col-lg-9"><?php echo form_input($company_name_attr) ?></div>
						</div><!-- ./row -->
						</br>
						<div class="row">
							<div class="col-lg-3"><?php echo form_label('Company Email'); ?></div>
							<div class="col-lg-9"><?php echo form_input($company_email_attr) ?></div>
						</div><!-- ./row -->
						</br>
						<div class="row">
							<div class="col-lg-3"><?php echo form_label('Company Phone'); ?></div>
							<div class="col-lg-9"><?php echo form_input($company_phone_attr) ?></div>
						</div><!-- ./row -->
						</br>
						<div class="row">
							<div class="col-lg-3"><?php echo form_label('Company Address'); ?></div>
							<div class="col-lg-9"><?php echo form_textarea($company_address_attr) ?></div>
						</div><!-- ./row -->
						</br>
						<div class="row">
							<div class="col-lg-3"><?php echo form_label('Company Google Map'); ?></div>
							<div class="col-lg-9"><?php echo form_textarea($company_google_map_attr) ?></div>
						</div><!-- ./row -->
						</br>
							
					</li>
				</ul><!-- ./list-group -->
			</div><!-- ./card-body -->
			<div class="card-footer text-center">
				<?php 
				echo form_submit($submit_configure_attr);
				echo form_close();
				?>
			</div><!-- ./card-footer -->
		</div><!-- ./card -->
	</div><!-- ./configure-page -->
</div><!-- ./main-content -->

<?php
// include dashboard_end
$this->load->view('template/dashboard_end');

// include footer
$this->load->view('template/footer');
?>