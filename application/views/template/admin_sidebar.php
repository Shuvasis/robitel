<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
?>

<div class="main-sidebar">
	<ul class="sidebar-menu list-group">
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-dashboard"></i>
			<a href="<?php echo base_url('dashboard'); ?>">Dashboard</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-gears"></i>
			<a href="<?php echo base_url('dashboard/configure'); ?>">Configure</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-edit"></i>
			<a href="<?php echo base_url('dashboard/about'); ?>">About</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-gear"></i>
			<a href="<?php echo base_url('dashboard/services'); ?>">Services</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-users"></i>
			<a href="<?php echo base_url('dashboard/users/'); ?>">Users</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-shopping-cart"></i>
			<a href="<?php echo base_url('dashboard/packages'); ?>">Packages</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-dollar"></i>
			<a href="<?php echo base_url('dashboard/billpay'); ?>">Billpay</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-feed"></i>
			<a href="<?php echo base_url('dashboard/blog'); ?>">Blog</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-question"></i>
			<a href="<?php echo base_url('dashboard/faq'); ?>">FAQ</a>
		</li>
	</ul>
</div><!-- ./main-sidebar -->			