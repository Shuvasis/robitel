<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title><?php echo $title . ' | Robitel'; ?></title>

	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>">
	<!-- Main Stylesheet -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css?'. time()); ?>">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
	<header class='main-header'>
		<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Navbar</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="<?php echo base_url('page'); ?>">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('page/about'); ?>">About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('page/services'); ?>">Services</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('page/packages'); ?>">Packages</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Media
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<?php echo base_url(''); ?>">FTP Server</a>
							<a class="dropdown-item" href="<?php echo base_url(''); ?>">Another action</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('page/billpay'); ?>">Billpay</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('page/blog'); ?>">Blog</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('page/faq'); ?>">FAQ</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('page/contact'); ?>">Contact</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownAccount" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Account
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownAccount">
							<?php if(!$this->session->logged_in) : ?>
								<a class="dropdown-item" href="<?php echo base_url(''); ?>">Login</a>
								<a class="dropdown-item" href="<?php echo base_url(''); ?>">Register</a>
							<?php else : ?>
								<a class="dropdown-item" href="<?php echo base_url(''); ?>">Dashboard</a>
								<a class="dropdown-item" href="<?php echo base_url(''); ?>">Logout</a>
							<?php endif; ?>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('user/login'); ?>">Login</a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0">
					<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>
			</div>
		</nav>
	</header>
