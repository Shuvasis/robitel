<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

	<footer class="main-footer container-fluid">
		<div class="container">
			<div class="row footer-menus">
				<div class="col-lg-3 col-sm-6 footer-menu">
					<p class="title">About Company</p>
					<ul class="list-group list-group-flush">
					    <li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">About the Company</a></li>
					    <li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">Company Mission</a></li>
					    <li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">Technology Partner</a></li>
	  					<li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">Support Partner</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6 footer-menu">
					<p class="title">Company Services</p>
					<ul class="list-group list-group-flush">
						<li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">About the Company</a></li>
					    <li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">Company Mission</a>
						<li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">Support Partner</a></li>
	  					<li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">Technology Partner</a></li>
	  				</ul>
				</div>
				<div class="col-lg-3 col-sm-6 footer-menu">
					<p class="title">Support</p>
					<ul class="list-group list-group-flush">
					    <li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">About the Company</a></li>
					    <li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">Company Mission</a></li>
					    <li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">Technology Partner</a></li>	  <li class="list-group-item"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="">Support Partner</a></li>
	  				</ul>
	  			</div>
				<div class="col-lg-3 col-sm-6 footer-menu contact">
					<p class="title">Contact</p>
					<ul class="list-group list-group-flush">
					    <li class="list-group-item"><i class="fa fa-institution" aria-hidden="true"></i><a>3rd Floor, Rijuan Complex, Riajuddin Bazar, New Market, Chittagong</a></li>
					    <li class="list-group-item"><i class="fa fa-phone" aria-hidden="true"></i><a>017XXXXXXX</a></li>
					    <li class="list-group-item"><i class="fa fa-envelope" aria-hidden="true"></i><a>admin@robitel.com</a></li>	
	  				</ul>
				</div>
			</div><!-- ./footer-menus -->
			<div class="footer-bottom row">
				<div class="col-lg-4 dev-credit text-left">
					<a href="">Developed by Shuvasis Datta </a>
				</div><!-- ./dev-credit -->
				<div class="col-lg-4 copyright text-center">
					<a href="">© Copyright RobiTel - <?php echo date('Y'); ?>. All Rights Reserved.</a>
				</div><!-- ./copyright -->
				<div class="col-lg-4 social-menu text-right">
					<a href="//www.facebook.com/">
						<i class="fa fa-facebook" aria-hidden="true"></i>
					</a>
					<a href="//www.linkedin.com">
						<i class="fa fa-linkedin" aria-hidden="true"></i>
					</a>
					<a href="" class="btn-sm btn-danger border-0 back-to-top text-right" onclick="$('html,body').animate({scrollTop:0},'slow');return false;" data-toggle="tooltip" data-placement="top" title="Back to Top">
						<i class="fa fa-arrow-up" aria-hidden="true"></i>
					</a>
				</div><!-- ./social-menu -->
				<div class="col-lg-3 back-to-top text-right">
				</div><!-- ./back-to-top -->
			</div><!-- ./footer-bottom -->
		</div><!-- ./container -->
	</footer>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
</body>
</html>