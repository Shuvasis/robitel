<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
?>

<div class="dashboard container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<?php 			
			if ($this->session->user->ugroup_name == 'Admin') {
				$this->load->view('template/admin_sidebar');
			} else {
				$this->load->view('template/client_sidebar');	
			}	
			?>
		</div>
		<div class="col-lg-9">
			<div class="dashboard-content">