<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
?>

<div class="main-sidebar">
	<ul class="sidebar-menu list-group">
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-dashboard"></i>
			<a href="<?php echo base_url('dashboard/user_profile/'. $this->session->user->user_id); ?>">Profile</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-shopping-cart"></i>
			<a href="<?php echo base_url('dashboard/my_package'); ?>">My Package</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-dollar"></i>
			<a href="<?php echo base_url('dashboard/billpay'); ?>">Billpay</a>
		</li>
		<li class="list-group-item sidebar-menu-item">
			<i class="fa fa-feed"></i>
			<a href="<?php echo base_url('dashboard/bill_history'); ?>">Bill History</a>
		</li>
	</ul>
</div><!-- ./main-sidebar -->			