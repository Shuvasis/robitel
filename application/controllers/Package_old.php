<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends CI_Controller {

	public function index() {
		$data['title'] = 'Packages';
		$data['packages'] = $this->Package_Model->get_packages();
		$this->load->view('pages/packages', $data);
	}

	public function admin() {
		$data['title'] = 'Packages';
		$data['packages'] = $this->Package_Model->get_packages();
		$this->load->view('admin/packages/view', $data);
	}

	// view single package 
	public function single_view($id) {
		$data['title'] = 'Single Package View';
		$data['package'] = $this->Package_Model->get_package($id);
		$this->load->view('admin/packages/single_view', $data);
	}

	// Create new package
	public function create() {
		// Process the form data
		$this->form_validation->set_rules('package_name', 'Package Name', 'required');
		$this->form_validation->set_rules('package_description', 'Package Description', 'required');
		$this->form_validation->set_rules('package_avg_speed', 'Package Speed', 'required');
		$this->form_validation->set_rules('package_youtube_speed', 'Youtube Speed', 'required');
		$this->form_validation->set_rules('package_price', 'Package Price', 'required');
		$this->form_validation->set_rules('package_img_src', 'Package Image', 'valid_url|required');
		
		if ($this->form_validation->run() == FALSE) {
			// Load Create Package View
			$data['title'] 	= 'Create Package';
			$data['errors'] = validation_errors();
			$this->load->view('admin/packages/create_package', $data);	
        } else {
        	$data['title'] = 'Packages';
        	$form_data	= array (
        		'package_name' 			=> $this->input->post('package_name'),
        		'package_description'	=> $this->input->post('package_description'),
        		'package_avg_speed' 	=> $this->input->post('package_avg_speed'),
        		'package_youtube_speed'	=> $this->input->post('package_youtube_speed'),
        		'package_price'			=> $this->input->post('package_price'),
        		'package_img_src'		=> $this->input->post('package_img_src')
        	);

        	// Pass the data to Package Model for creating new Package
        	$this->Package_Model->create_package($form_data);

        	// Redirect to Package View 
    		redirect('package');	
        }
	}

	public function edit($id = -1) {
		// Process the form data
		$this->form_validation->set_rules('package_name', 'Package Name', 'required');
		$this->form_validation->set_rules('package_description', 'Package Description', 'required');
		$this->form_validation->set_rules('package_avg_speed', 'Package Speed', 'required');
		$this->form_validation->set_rules('package_youtube_speed', 'Youtube Speed', 'required');
		$this->form_validation->set_rules('package_price', 'Package Price', 'required');
		$this->form_validation->set_rules('package_thumbnail', 'Package Thumbnail Image', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			// Load Edit Package View
			$data['title'] = 'Edit Package';
			$data['package'] = $this->Package_Model->get_package($id);
			$data['errors'] = validation_errors();
			$this->load->view('admin/packages/edit_package', $data);	
        } else {
        	$data['title']	= 'Packages';
        	$to_update_data	= array (
        		'package_name' 			=> $this->input->post('package_name'),
        		'package_description'	=> $this->input->post('package_description'),
        		'package_avg_speed' 	=> $this->input->post('package_avg_speed'),
        		'package_youtube_speed'	=> $this->input->post('package_youtube_speed'),
        		'package_price'			=> $this->input->post('package_price'),
        		'package_thumbnail'		=> $this->input->post('package_thumbnail')
        	);

        	// Pass the data to Package Model for creating new Package
        	$this->Package_Model->update_package($id, $to_update_data);

        	// Redirect to Admin Package View 
    		redirect('package/admin');	
    	}	
	}

	public function delete($id = -1) {
		$data['title'] = 'Delete Package';
		$data['package'] = $this->Package_Model->delete_package($id);

       	// Redirect to Admin Package View 
		redirect('package');
	}
}
