<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index() {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		$data['title'] = 'Dashboard';
		$this->load->view('admin/dashboard', $data);
	}

	// Checks if user is logged in or not. if not logged in then redirect to the home page, else gives permission for access 
	private function check_dashboard_access_permission() {
		if (!$this->session->logged_in) {
			// redirect to the home page
			redirect('page/home');
		}
	}

	// method for uploading files
	private function do_upload($file_field_name, $upload_sub_direcotry, $allowed_types) {
		$upload_path = 'uploads/'.$upload_sub_direcotry;
		
		// if folder not exists make it
		if(!file_exists($upload_path)) {
			mkdir($upload_path);
		}

		$config['upload_path']          = $upload_path;
		$config['allowed_types']        = $allowed_types;
		$config['max_size']             = '2048';
		$config['max_width']            = '1024';
		$config['max_height']           = '768';
		
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload($file_field_name))
		{
			return (object)array( 'error' => $this->upload->display_errors());
		}
		else
		{
			return (object)array('file_info' => $this->upload->data());
		}
	}

	/****************** Configure ****************/
	// configure website 
	public function configure() {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		// form validation
		$this->form_validation->set_rules('company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('company_phone', 'Company Phone', 'required');
		$this->form_validation->set_rules('company_email', 'Company Email', 'required|valid_email');
		$this->form_validation->set_rules('company_address', 'Company Address', 'required');
		$this->form_validation->set_rules('company_google_map', 'Company Google Map', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$data['title'] 		= 'Configure';
			$data['errors'] 	= validation_errors();
			// gets & passed all configuration data to the view
			$data['configure']	= $this->Configure_Model->get_metadata();
			$this->load->view('admin/configure', $data);	
		} else {	
			$form_data = array(
				array(
					'meta_key'		=> 'company_name',
					'meta_value'	=> $this->input->post('company_name')
				),
				array(
					'meta_key'		=> 'company_email',
					'meta_value'	=> $this->input->post('company_email')
				),
				array(
					'meta_key'		=> 'company_phone',
					'meta_value'	=> $this->input->post('company_phone')
				),
				array(
					'meta_key'		=> 'company_address',
					'meta_value'	=> $this->input->post('company_address')
				),
				array(
					'meta_key'		=> 'company_google_map',
					'meta_value'	=> $this->input->post('company_google_map')
				)
			);
			
			// insert configure data intto meta table 
			$this->Configure_Model->replace_metadata($form_data);	

			// redirect to configure page
			redirect('dashboard/configure');
		}
	} 


	/****************** User ****************/
	// View users
	public function users() {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		$data['title'] = 'Users';
		$data['users'] = $this->User_Model->get_users();
		$this->load->view('admin/users/view', $data);
	}

	// User Profile
	public function user_profile($id = -1) {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		$data['title'] = 'User Profile';
		
		// Get the user data by its id
		$data['user'] = $this->User_Model->get_user($id);
		$this->load->view('admin/users/profile', $data);
	}

	// Add User
	public function user_add() {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		// form validation
		$this->form_validation->set_rules('user_name', 'Name', 'required');
		$this->form_validation->set_rules('user_phone', 'Phone', 'required');
		$this->form_validation->set_rules('user_address', 'Address', 'required');
		$this->form_validation->set_rules('ugroup_id', 'Group', 'required');
		$this->form_validation->set_rules('user_status', 'Status', 'required');
		$this->form_validation->set_rules('user_img', 'Image', 'required');
		

		if ($this->form_validation->run() == FALSE) {
			$data['title'] = 'Edit User';
			$data['errors'] = validation_errors();
			
			// Get the user data by its id
			$data['user'] = $this->User_Model->get_user($id);
			
			// Get all user groups 
			$data['user_groups'] = $this->User_Model->get_user_groups();

			$this->load->view('admin/users/edit', $data);	
		} else {

			$form_data_1 = array(
				'user_status'	=> $this->input->post('user_status')
			);

			$form_data_2 = array(
				'user_name'		=> $this->input->post('user_name'),
				'user_phone'	=> $this->input->post('user_phone'),
				'user_address'	=> $this->input->post('user_address'),
				'user_img'		=> $this->input->post('user_img'),
				'ugroup_id'		=> $this->input->post('ugroup_id')
			);
			
			// create user with submitted data
			$this->User_Model->update_user($id, $form_data_1, $form_data_2);	

			// redirect to login page
			redirect('dashboard/users');
		}
	} 

	// Edit User
	public function user_edit($id = -1) {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		// form validation
		$this->form_validation->set_rules('user_name', 'Name', 'required');
		$this->form_validation->set_rules('user_phone', 'Phone', 'required');
		$this->form_validation->set_rules('user_address', 'Address', 'required');
		$this->form_validation->set_rules('ugroup_id', 'Group', 'required');
		$this->form_validation->set_rules('user_status', 'Status', 'required');
		// $this->form_validation->set_rules('user_img', 'Image', 'required');
		

		if ($this->form_validation->run() == FALSE) {
			$data['title'] = 'Edit User';
			$data['errors'] = validation_errors();
			
			// Get the user data by its id
			$data['user'] = $this->User_Model->get_user($id);
			
			// Get all user groups 
			$data['user_groups'] = $this->User_Model->get_user_groups();

			$this->load->view('admin/users/edit', $data);	
		} else {

			$form_data_1 = array(
				'user_status'	=> $this->input->post('user_status')
			);

			$form_data_2 = array(
				'user_name'		=> $this->input->post('user_name'),
				'user_phone'	=> $this->input->post('user_phone'),
				'user_address'	=> $this->input->post('user_address'),
				'user_img'		=> $this->input->post('user_img'),
				'ugroup_id'		=> $this->input->post('ugroup_id')
			);
			
			// create user with submitted data
			$this->User_Model->update_user($id, $form_data_1, $form_data_2);	

			// redirect to login page
			redirect('dashboard/users');
		}		
	}

	// Delete User 
	public function user_delete($id) {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		$this->User_Model->delete_user($id);

		// redirect to Admin Dashboard -> user
		redirect('dashboard/users'); 
	}

	/****************** Packages ****************/

	// View Packages
	public function packages() {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		$data['title'] = 'Packages';
		$data['packages'] = $this->Package_Model->get_packages();
		$this->load->view('admin/packages/view', $data);
	}

	// view single package 
	public function package_overview($id) {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		$data['title'] = 'Single Package View';
		$data['package'] = $this->Package_Model->get_package($id);
		$this->load->view('admin/packages/single_view', $data);
	}

	// Create new package
	public function create_package() {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		// Process the form data
		$this->form_validation->set_rules('package_name', 'Package Name', 'required');
		$this->form_validation->set_rules('package_description', 'Package Description', 'required');
		$this->form_validation->set_rules('package_avg_speed', 'Package Speed', 'required');
		$this->form_validation->set_rules('package_youtube_speed', 'Youtube Speed', 'required');
		$this->form_validation->set_rules('package_price', 'Package Price', 'required');
		$this->form_validation->set_rules('package_thumbnail', 'Package Thumbnail Image', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			// Load Create Package View
			$data['title'] 	= 'Create Package';
			$data['errors'] = validation_errors();
			$this->load->view('admin/packages/create_package', $data);	
		} else {
			$data['title'] = 'Packages';
			$form_data	= array (
				'package_name' 			=> $this->input->post('package_name'),
				'package_description'	=> $this->input->post('package_description'),
				'package_avg_speed' 	=> $this->input->post('package_avg_speed'),
				'package_youtube_speed'	=> $this->input->post('package_youtube_speed'),
				'package_price'			=> $this->input->post('package_price'),
				'package_thumbnail'		=> $this->input->post('package_thumbnail')
			);

        	// Pass the data to Package Model for creating new Package
			$this->Package_Model->create_package($form_data);

        	// Redirect to Package View 
			redirect('package');	
		}
	}

	// Edit Package
	public function edit_package($id = -1) {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		// Process the form data
		$this->form_validation->set_rules('package_name', 'Package Name', 'required');
		$this->form_validation->set_rules('package_description', 'Package Description', 'required');
		$this->form_validation->set_rules('package_avg_speed', 'Package Speed', 'required');
		$this->form_validation->set_rules('package_youtube_speed', 'Youtube Speed', 'required');
		$this->form_validation->set_rules('package_price', 'Package Price', 'required');

		// Upolad File Validation		
		$upload_error = FALSE;
		if(empty($_FILES['package_thumbnail']['name'])) {
			$this->form_validation->set_rules('package_thumbnail', 'Package Thumbnail Image', 'required');
		} else {
			$upload = $this->do_upload('package_thumbnail', 'packages', 'gif|jpg|png');
			if(isset($upload->error)) {
				$upload_error = True;
			}
		} 

		if ($this->form_validation->run() == FALSE || $upload_error == TRUE) {
			// Load Edit Package View
			$data['title'] = 'Edit Package';
			$data['package'] = $this->Package_Model->get_package($id);
			$data['errors'] = validation_errors();
			
			// Pass upload error if it has
			if($upload_error) {
				$data['errors'] .= '</br>'. $upload->error;
			}

			$this->load->view('admin/packages/edit_package', $data);	
		} else {
			$data['title']	= 'Packages';
			$to_update_data	= array (
				'package_name' 			=> $this->input->post('package_name'),
				'package_description'	=> $this->input->post('package_description'),
				'package_avg_speed' 	=> $this->input->post('package_avg_speed'),
				'package_youtube_speed'	=> $this->input->post('package_youtube_speed'),
				'package_price'			=> $this->input->post('package_price'),
				'package_thumbnail'		=> base_url('uploads/packages/'.$upload->file_info['file_name'])
			);

        	// Pass the data to Package Model for creating new Package
			$this->Package_Model->update_package($id, $to_update_data);

        	// Redirect to Admin Package View 
			redirect('dashboard/packages');	
		}	
	}

	// Delete Package
	public function delete_package($id = -1) {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		$data['title'] = 'Delete Package';
		$data['package'] = $this->Package_Model->delete_package($id);

       	// Redirect to Admin Package View 
		redirect('dashboard/packages');
	}

	/******************* FAQ *******************/

	// View All FAQ
	public function faq() {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();

		$data['title'] = 'FAQ (Frequently Asked Question)';
		$this->load->view('admin/faq/view', $data);
	} 

	// Create New FAQ
	public function create_faq() {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();
		
	}

	// Create New FAQ
	public function edit_faq($id) {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();
		
	}

	// Create New FAQ
	public function delete_faq($id) {
		// check dashboard function access permission
		$this->check_dashboard_access_permission();
		
	} 
}