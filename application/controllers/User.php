<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function index() {
		redirect('user/login');
	}

	// redirect the logged in user to their corresponding dashboard
	private function conrrespoding_dashboard_redirect() {
		if ($this->session->user->ugroup_name == 'Admin') {
				// redirect to Admin Dashboard
			redirect('dashboard');	
		} else {
				// redirect to user profile page
			redirect('dashboard/user_profile/'.$this->session->user->user_id);
		}
	}

	// User Login Method
	public function login() {
		if ($this->session->logged_in) {
			$this->conrrespoding_dashboard_redirect();
		} else {
			$this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('user_password', 'Password', 'required');

			$data['title'] = 'Login Panel';

			if ($this->form_validation->run() == FALSE) {
				$data['errors'] = validation_errors();
				$this->load->view('pages/login', $data);
			} else {
				$form_data = array(
					'user_email' 	=> $this->input->post('user_email'),
					'user_password' => $this->input->post('user_password')
				);

				if ($user = $this->User_Model->user_exist($form_data)) {
					$session_data = array(
						'user'		=>	$user,
						'logged_in'	=> True
					);

					// If gets the user, start user Session
					$this->session->set_userdata($session_data);

					// redirect to conrrespoding dashboard
					$this->conrrespoding_dashboard_redirect();
				} else {
				// Else Show a Not Found Message to the form.
					$data['errors'] = 'User not found !';
					$this->load->view('pages/login', $data);
				}
			}			
		}
	}

	// User Logout Method
	public function logout() {
		// Destroy session
		$this->session->sess_destroy();
		
		// redirect to home page
		redirect(base_url());
	}

	// User Register Method
	public function register() {
		if (!$this->session->logged_in) {
			// form validation
			$this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[users.user_email]', array('is_unique' => 'Email already exists'));
			$this->form_validation->set_rules('user_password', 'Password', 'required|min_length[5]');
			$this->form_validation->set_rules('user_confirm_password', 'Confirm Password', 'required|matches[user_password]');
			$this->form_validation->set_rules('user_name', 'Name', 'required');
			$this->form_validation->set_rules('user_phone', 'Phone', 'required');
			$this->form_validation->set_rules('user_address', 'Address', 'required');
			// $this->form_validation->set_rules('user_img', 'Image', 'required');
			

			if ($this->form_validation->run() == FALSE) {
				$data['title'] = 'User Register';
				$data['errors'] = validation_errors();
				$this->load->view('pages/register', $data);
			} else {
				$form_data_1 = array(
					'user_email'	=> $this->input->post('user_email'),
					'user_password'	=> $this->input->post('user_password')
				);

				$form_data_2 = array(
					'user_name'		=> $this->input->post('user_name'),
					'user_phone'	=> $this->input->post('user_phone'),
					'user_address'	=> $this->input->post('user_address'),
					'user_img'		=> $this->input->post('user_img')
				);

				// create user with submitted data
				$this->User_Model->create_user($form_data_1, $form_data_2);	

				// redirect to login page
				redirect('user/login');
			}
		} else {
			// redirect to home page.
			redirect('dashboard/home');
		}
	}
}