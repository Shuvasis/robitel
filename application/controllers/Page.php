<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index() {
		$data['title'] = 'Home';
		$this->load->view('pages/home', $data);
	}

	public function home() {
		// redirect to the home.
		redirect('page');
	}

	/* Contact Page */
	public function contact() {
		$data['title'] = 'Contact';
		$this->load->view('pages/contact', $data);	
	}

	/* Services Page */
	public function services() {
		$data['title'] = 'Services';
		$this->load->view('pages/services', $data);	
	}

	/* About Page */
	public function about() {
		$data['title'] = 'About';
		$this->load->view('pages/about', $data);	
	}

	/* FAQ Page */
	public function faq() {
		$data['title'] = 'FAQ (Frequently Asked Question)';
		$this->load->view('pages/faq', $data);	
	}

	/* Packages Page */
	public function packages() {
		$data['title'] = 'Packages';
		$data['packages'] = $this->Package_Model->get_packages();
		$this->load->view('pages/packages', $data);	
	}
	
	/* Login Page */
	public function login() {
		$data['title'] = 'Login Panel';
		$this->load->view('pages/login', $data);	
	}

}